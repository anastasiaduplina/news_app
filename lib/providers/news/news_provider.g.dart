// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$newsStateHash() => r'e32ba88df51ffc70fa3b98cfbf2ca911c5cb7b99';

/// See also [NewsState].
@ProviderFor(NewsState)
final newsStateProvider =
    AutoDisposeAsyncNotifierProvider<NewsState, News>.internal(
  NewsState.new,
  name: r'newsStateProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$newsStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$NewsState = AutoDisposeAsyncNotifier<News>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
