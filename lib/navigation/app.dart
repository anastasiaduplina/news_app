import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:news/navigation/screens/home.dart';
import 'package:news/providers/locale/localization_provider.dart';
import 'package:news/providers/theme/theme_provider.dart';

import '../themes/dark_theme.dart';
import '../themes/light_theme.dart';

class MyApp extends ConsumerWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return MaterialApp(
      title: 'News app',
      theme: lightTheme(),
      darkTheme: darkTheme(),
      themeMode:
          ref.watch(themeDarkStateProvider) ? ThemeMode.dark : ThemeMode.light,
      debugShowCheckedModeBanner: false,
      home: const MyHomePage(),
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      locale: ref.watch(localizationStateProvider),
      supportedLocales: const [
        Locale('en'),
        Locale('ru'),
      ],
    );
  }
}
