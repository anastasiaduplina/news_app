import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:infinite_scroll/infinite_scroll_list.dart';
import 'package:news/dto/news.dart';
import 'package:news/navigation/screens/info.dart';
import 'package:news/providers/locale/localization_provider.dart';
import 'package:news/providers/news/news_provider.dart';
import 'package:news/providers/theme/theme_provider.dart';

class MyHomePage extends ConsumerStatefulWidget {
  const MyHomePage({super.key});

  @override
  MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends ConsumerState<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    bool dark = ref.watch(themeDarkStateProvider);
    Locale locale = ref.watch(localizationStateProvider);
    News? newsProv = ref.watch(newsStateProvider).value;
    News news = newsProv ?? const News('', 0, []);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context)!.news,
          style: Theme.of(context).textTheme.titleLarge,
        ),
        actions: [
          TextButton(
            onPressed: () {
              ref.read(localizationStateProvider.notifier).setLocale(
                    locale: Locale((locale.languageCode == 'en') ? 'ru' : 'en'),
                  );
            },
            child: Text(
              locale.languageCode.toUpperCase(),
              style: Theme.of(context).textTheme.labelLarge,
            ),
          ),
          IconButton(
            icon: dark ? const Icon(Icons.nightlight) : const Icon(Icons.sunny),
            onPressed: () {
              ref.read(themeDarkStateProvider.notifier).changeTheme();
            },
          ),
        ],
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: InfiniteScrollList(
            physics: const BouncingScrollPhysics(),
            shrinkWrap: true,
            onLoadingStart: (page) async {
              ref
                  .read(newsStateProvider.notifier)
                  .addNewNews(page: page + 1, oldNews: news);
            },
            children: news.articles!
                .map(
                  (e) => NewsWidget(
                    url: (e.urlToImage != null) ? e.urlToImage.toString() : ' ',
                    date:
                        (e.publishedAt != null) ? e.publishedAt.toString() : ' ',
                    title: (e.title != null) ? e.title.toString() : ' ',
                    description:
                        (e.description != null) ? e.description.toString() : ' ',
                    urlToSource: (e.url != null) ? e.url.toString() : ' ',
                  ),
                )
                .toList(),
          ),
        ),
      ),
    );
  }
}

class NewsWidget extends StatelessWidget {
  const NewsWidget({
    super.key,
    required this.url,
    required this.date,
    required this.title,
    required this.description,
    required this.urlToSource,
  });

  final String url;
  final String date;
  final String title;
  final String description;
  final String urlToSource;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Information(
              url: url,
              title: title,
              text: description,
              urlToSource: urlToSource,
            ),
          ),
        );
      },
      child: Card(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(6)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CachedNetworkImage(
                imageUrl: url,
                placeholder: (context, url) =>
                    const CircularProgressIndicator(),
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
              Text(date, style: Theme.of(context).textTheme.labelSmall),
              Text(title, style: Theme.of(context).textTheme.titleMedium),
            ],
          ),
        ),
      ),
    );
  }
}
