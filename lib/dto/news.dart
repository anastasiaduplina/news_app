import 'package:json_annotation/json_annotation.dart';

import 'new_one.dart';

part 'news.g.dart';

@JsonSerializable()
class News {
  final String? status;
  final int? totalResults;
  final List<NewOne>? articles;

  const News(this.status, this.totalResults, this.articles);

  factory News.fromJson(Map<String, dynamic> json) => _$NewsFromJson(json);

  Map<String, dynamic> toJson() => _$NewsToJson(this);
}
