import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:news/dto/new_one.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

import '../../dto/news.dart';
import '../../utils/utils.dart';

part 'news_provider.g.dart';

@riverpod
class NewsState extends _$NewsState {
  @override
  Future<News> build() async {
    News news = await getNewsData(1);
    return news;
  }

  Future<void> updateNews() async {
    News newNews;
    try {
      newNews = await getNewsData(1);
    } catch (e) {
      newNews = const News('null', 0, []);
    }
    state = AsyncValue.data(newNews);
    // return newNews;
  }

  Future<News> addNewNews({required int page, required News oldNews}) async {
    News newNews;
    try {
      newNews = await getNewsData(page);
    } catch (e) {
      newNews = const News('null', 0, []);
    }
    newNews = News(
      newNews.status,
      newNews.totalResults,
      List.from(oldNews.articles as Iterable<NewOne>)
        ..addAll(newNews.articles as Iterable<NewOne>),
    );
    state = AsyncValue.data(newNews);
    return newNews;
  }
}

Future<News> getNewsData(int page) async {
  final newsDataUrl = Uri.parse(
    '$url&page=$page',
  );
  final newsDataResponse = await http.get(newsDataUrl);
  if (newsDataResponse.statusCode != 200) {
    throw Exception('Failed to load data');
  }
  News news = News.fromJson(jsonDecode(newsDataResponse.body));
  return news;
}
