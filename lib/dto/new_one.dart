import 'package:json_annotation/json_annotation.dart';
import 'package:news/dto/source.dart';

part 'new_one.g.dart';

@JsonSerializable()
class NewOne {
  final Source? source;
  final String? author;
  final String? title;
  final String? description;
  final String? url;
  final String? urlToImage;
  final String? publishedAt;
  final String? content;

  const NewOne(
    this.source,
    this.author,
    this.title,
    this.description,
    this.url,
    this.urlToImage,
    this.publishedAt,
    this.content,
  );

  factory NewOne.fromJson(Map<String, dynamic> json) => _$NewOneFromJson(json);

  Map<String, dynamic> toJson() => _$NewOneToJson(this);
}
