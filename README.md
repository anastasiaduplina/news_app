
# News App
Это приложение, которое позволяет пользователям просматривать список новостей и просматривать подробную информацию о каждой выбранной новости. Приложение состоит из двух экранов:


### Главный экран:
На этом экране отображается список новостей. Пользователи могут прокручивать список, чтобы увидеть доступные новостные позиции. При достижении конца списка подгружается еще некоторое количество новостей.


### Экран подробной информации:
Когда пользователь нажимает на новостную позицию на главном экране, он попадает на экран деталей. На этом экране предоставляется более подробная информация о выбранной новостной позиции.

### Технологии
Для получения новостей использовалось API: https://newsapi.org/
Также использовался Riverpod для State management

## Screenshots
![App Screenshot](screenshots/photo1.jpg)
### Темная тема:
![App Screenshot](screenshots/photo2.jpg)
### Смена языка:
![App Screenshot](screenshots/photo3.jpg)
### Подробная информация:
![App Screenshot](screenshots/photo4.jpg)


## Deployment

Начало работы
Чтобы запустить локальную копию, выполните следующие простые шаги:


Клонируйте репозиторий:
```bash
  git clone https://gitlab.com/anastasiaduplina/news_app.git
```

Перейдите в каталог проекта:
```bash
  cd your-repo-name
``` 
Установите зависимости:
```bash
   Flutter flutter pub get
```
Запустите приложение:
```bash
  flutter run
```
## APK:
https://disk.yandex.ru/d/evlW0w_LEdKNEQ

