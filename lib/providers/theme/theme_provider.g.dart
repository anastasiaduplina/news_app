// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'theme_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$themeDarkStateHash() => r'fe0e1c5a1f251ad40a292c45dea1dc2a91d0bad0';

/// See also [ThemeDarkState].
@ProviderFor(ThemeDarkState)
final themeDarkStateProvider =
    AutoDisposeNotifierProvider<ThemeDarkState, bool>.internal(
  ThemeDarkState.new,
  name: r'themeDarkStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$themeDarkStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ThemeDarkState = AutoDisposeNotifier<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
