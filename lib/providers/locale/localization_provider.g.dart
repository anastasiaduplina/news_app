// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'localization_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$localizationStateHash() => r'3573a5ca010190104065bc626918906d8f4cacc2';

/// See also [LocalizationState].
@ProviderFor(LocalizationState)
final localizationStateProvider =
    AutoDisposeNotifierProvider<LocalizationState, Locale>.internal(
  LocalizationState.new,
  name: r'localizationStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$localizationStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LocalizationState = AutoDisposeNotifier<Locale>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
