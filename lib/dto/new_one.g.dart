// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'new_one.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewOne _$NewOneFromJson(Map<String, dynamic> json) => NewOne(
      json['source'] == null
          ? null
          : Source.fromJson(json['source'] as Map<String, dynamic>),
      json['author'] as String?,
      json['title'] as String?,
      json['description'] as String?,
      json['url'] as String?,
      json['urlToImage'] as String?,
      json['publishedAt'] as String?,
      json['content'] as String?,
    );

Map<String, dynamic> _$NewOneToJson(NewOne instance) => <String, dynamic>{
      'source': instance.source,
      'author': instance.author,
      'title': instance.title,
      'description': instance.description,
      'url': instance.url,
      'urlToImage': instance.urlToImage,
      'publishedAt': instance.publishedAt,
      'content': instance.content,
    };
